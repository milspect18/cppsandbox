function(LinkTarget build_target link_type link_target link_dir)
    if(TARGET ${link_target})
        message("-- ${link_target} module found")
        target_link_libraries(${build_target} ${link_type} ${link_target})
    elseif(EXISTS ${link_dir}/CMakeLists.txt)
        add_subdirectory(${link_dir} ${CMAKE_CURRENT_BINARY_DIR}/${link_target})
        target_link_libraries(${build_target} ${link_type} ${link_target})
    endif(TARGET ${link_target})
endfunction(LinkTarget)

