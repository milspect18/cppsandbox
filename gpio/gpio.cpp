/*! \brief C++ class for sysfs gpio control
 */

#include "gpio.hpp"
#include <utility>
#include <fstream>
#include <iostream>


namespace KP_GPIO {


Gpio::Gpio(Gpio &&other) :
pinNum(std::move(other.pinNum)),
pinDir(std::move(other.pinDir)),
pinState(std::move(other.pinState))
{ 
    other.pinNum = 255;
    
    if (!GPIO_UTILS::gpioIsExported(pinNum)) {
        GPIO_UTILS::exportGpio(pinNum);
    }
}


Gpio::Gpio(uint8_t initPinNum, GPIO_UTILS::State initState, GPIO_UTILS::Direction initDir) :
pinNum(initPinNum),
pinState(initState),
pinDir(initDir)
{ 
    if (!GPIO_UTILS::gpioIsExported(initPinNum)) {
        GPIO_UTILS::exportGpio(initPinNum);
    }

    GPIO_UTILS::setGpioDirection(initPinNum, initDir);
    GPIO_UTILS::setGpioValue(initPinNum, initState);
}


Gpio::~Gpio() {
    // Unexport the pin!
    if (GPIO_UTILS::gpioIsExported(this->pinNum)) {
        std::cout << "Unexporting!" << std::endl;
        GPIO_UTILS::unExportGpio(this->pinNum);
    }
}


bool Gpio::setDirection(GPIO_UTILS::Direction dir) { 
    if (GPIO_UTILS::setGpioDirection(this->pinNum, dir)) {
        this->pinDir = dir;
        return true;
    } else {
        return false;
    }
}


bool Gpio::setState(GPIO_UTILS::State state) { 
    if (GPIO_UTILS::setGpioValue(this->pinNum, state)) {
        this->pinState = state;
        return true;
    } else {
        return false;
    }
}


GPIO_UTILS::Direction Gpio::getDirection() const { 
    return this->pinDir;
}


GPIO_UTILS::State Gpio::getState() const { 
    return this->pinState;
}

}