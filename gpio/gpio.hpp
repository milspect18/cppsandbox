/*! \brief C++ class for sysfs gpio control
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "gpioUtils.hpp"
#include <stdint.h>
#include <string>

namespace KP_GPIO {

    class Gpio {
        public:
            Gpio() = delete;
            Gpio(const Gpio &other) = delete;
            Gpio(Gpio &&other);
            explicit Gpio(uint8_t initPinNum) : Gpio(initPinNum, GPIO_UTILS::State::LOW, GPIO_UTILS::Direction::IN) {};
            Gpio(uint8_t initPinNum, GPIO_UTILS::State initState, GPIO_UTILS::Direction initDir);
            ~Gpio();

            bool setDirection(GPIO_UTILS::Direction dir);
            bool setState(GPIO_UTILS::State state);
            GPIO_UTILS::Direction getDirection() const;
            GPIO_UTILS::State getState() const;
        
        private:
            uint8_t pinNum;
            GPIO_UTILS::Direction pinDir;
            GPIO_UTILS::State pinState;
    };

}

#endif