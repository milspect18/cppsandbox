
#include "gpioUtils.hpp"
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>


namespace GPIO_UTILS {
    bool gpioIsExported(uint8_t gpioNum) {
        using namespace std;

        string pathname = SYSFS_BASE + "gpio" + to_string(gpioNum);
        struct stat info;
        bool retVal = false;

        if(stat(pathname.c_str(), &info) != -1) {
            cout << "Cannot access the gpio dir, are you sudo?" << endl;
            exit(1);
        } else if(info.st_mode & S_IFDIR) { 
            retVal = true;
        } else {
            retVal = false;
        }

        return retVal;
    }


    bool exportGpio(uint8_t gpioNum) {
        using namespace std;

        string pathname = SYSFS_BASE + "export";
        bool retVal = false;

        try {
            ofstream gpioExport(pathname);
            gpioExport << to_string(gpioNum);
            gpioExport.close();
            retVal = true;
        } catch (std::exception &e) {
            cout << "Unable to export gpio" + to_string(gpioNum) + ", are you sudo?" << endl;
            exit(1);
        }

        return true;
    }


    bool unExportGpio(uint8_t gpioNum) {
        using namespace std;

        string pathname = SYSFS_BASE + "unexport";
        bool retVal = false;

        try {
            ofstream gpioUnExport(pathname);
            gpioUnExport << to_string(gpioNum);
            gpioUnExport.close();
            retVal = true;
        } catch (std::exception &e) {
            cout << "Unable to unexport gpio" + to_string(gpioNum) + ", are you sudo?" << endl;
            exit(1);
        }

        return true;
    }


    bool setGpioDirection(uint8_t gpioNum, Direction dir) {
        using namespace std;

        string gpioFilePath = "gpio" + to_string(gpioNum);
        string pathname = SYSFS_BASE + gpioFilePath + "/direction";
        bool retVal = false;
        string dirStr;

        switch (dir) {
            case Direction::IN:
                dirStr = "in";
                break;
            case Direction::OUT:
                dirStr = "out";
                break;
            default:
                cout << "Unknown pin direction specified!" << endl;
                exit(1);
        }

        try {
            ofstream gpioDir(pathname);
            gpioDir << dirStr;
            gpioDir.close();
            retVal = true;
        } catch (std::exception &e) {
            cout << "Unable to set gpio" + to_string(gpioNum) + " direction, are you sudo?" << endl;
            exit(1);
        }

        return true;
    }


    bool setGpioValue(uint8_t gpioNum, State val) {
        using namespace std;

        string gpioFilePath = "gpio" + to_string(gpioNum);
        string pathname = SYSFS_BASE + gpioFilePath + "/value";
        bool retVal = false;

        try {
            ofstream gpioVal(pathname);
            gpioVal << to_string(static_cast<int>(val));
            gpioVal.close();
            retVal = true;
        } catch (std::exception &e) {
            cout << "Unable to set gpio" + to_string(gpioNum) + " value, are you sudo?" << endl;
            exit(1);
        }

        return true;
    }
}