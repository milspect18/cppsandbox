/*! \brief Utilities to help out with the sysfs gpio class
 */

#ifndef GPIO_UTILS_H_
#define GPIO_UTILS_H_

#include <stdint.h>
#include <string>

namespace GPIO_UTILS {
    const std::string SYSFS_BASE = "/sys/class/gpio/";

    enum class State : unsigned char  {
        HIGH = 1,
        LOW = 0
    };

    enum class Direction : unsigned char {
        OUT = 1,
        IN = 0
    };

    bool gpioIsExported(uint8_t gpioNum);
    bool exportGpio(uint8_t gpioNum);
    bool unExportGpio(uint8_t gpioNum);
    bool setGpioDirection(uint8_t gpioNum, Direction dir);
    bool setGpioValue(uint8_t gpioNum, State val);
}

#endif