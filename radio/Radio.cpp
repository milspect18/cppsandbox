
#include "Radio.hpp"
#include <iostream>



Radio::Radio() {
    this->config.initConfig();
}


void Radio::printConfig() {
    using namespace std;

    cout << this->config.getModel() << " Config Settings:" << endl;
    cout << "\tMode: " << this->config.getMode() << endl;
    cout << "\tRx Frequency: " << this->config.getRxFreq() << endl;
    cout << "\tRx Data Rate: " << this->config.getDataRate() << endl;
}