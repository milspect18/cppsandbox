
#ifndef RADIO_HPP
#define RADIO_HPP

#include "RadioConfig.hpp"
#include <string>

class Radio {
    public:
        Radio();
        ~Radio() {};

        void printConfig();

    private:
        std::string lstMsg = "";
        
        RadioConfig config;
};

#endif