
#include "RadioConfig.hpp"
#include "RadioConfigParser.hpp"
#include <map>


RadioConfig::RadioConfig() :
    rxFreq(UINT32_MAX),
    dataRate(UINT32_MAX),
    mode(UINT32_MAX),
    model("")
{ }


bool RadioConfig::initConfig() {
    std::map<std::string, std::string> configMap;

    RadioConfigParser::parseConfig(configMap);

    for (auto pair : configMap) {
        if (pair.first == "rxFreq") {
            this->rxFreq = static_cast<unsigned int>(stoi(pair.second));
        } else if (pair.first == "dataRate") {
            this->dataRate = static_cast<unsigned int>(stoi(pair.second));
        } else if (pair.first == "mode") {
            this->mode = static_cast<unsigned int>(stoi(pair.second));            
        } else if (pair.first == "model") {
            this->model = pair.second;
        }
    }

    return true;
}