
#ifndef RADIO_CONFIG_HPP
#define RADIO_CONFIG_HPP

#include <string>

struct RadioConfig {
    public:
        RadioConfig();
        ~RadioConfig() {};
        
        bool initConfig();

        // Inline definitions (getters/setters)
        unsigned int getRxFreq() const { return this->rxFreq; };
        unsigned int getDataRate() const { return this->dataRate; };
        unsigned int getMode() const { return this->mode; };
        std::string getModel() const { return this->model; };
        void setRxFreq(unsigned int newFreq) { this->rxFreq = newFreq; };
        void setDataRate(unsigned int newDataRate) { this->dataRate = newDataRate; };
        void setMode(unsigned int newMode) { this->mode = newMode; };
        void setModel(std::string newModel) { this->model = newModel; };

    private:
        unsigned int rxFreq;
        unsigned int dataRate;
        unsigned int mode;
        std::string model;
};

#endif