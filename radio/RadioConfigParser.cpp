
#include "RadioConfigParser.hpp"
#include <iostream>


bool RadioConfigParser::parseConfig(std::map<std::string, std::string> &mapRef) {
    // entire json tree
    Json::Value root;
    Json::Value configParams;
    std::ifstream configFile("config.json", std::ifstream::in);

    try {
        if (!getParamsFromFile(configFile, root, configParams)) {
            std::cout << "unable to parse params from file" << std::endl;
            return false;
        }

        fillMap(mapRef, configParams);

        return true;

    } catch (std::exception &e) {
        std::cout << e.what() << std::endl;
        return false;
    }
}


bool RadioConfigParser::getParamsFromFile(std::ifstream &file, 
                                          Json::Value &root, 
                                          Json::Value &params) {
    try {
        file >> root;
        params = root["config_params"];

        return true;

    } catch (std::exception &e) {
        std::cout << e.what() << std::endl;
        return false;
    }
}


void RadioConfigParser::fillMap(std::map<std::string, std::string> &mapRef, Json::Value &val) {
    for (Json::Value::ArrayIndex i = 0; i < val.size(); i++) {
            std::string paramName = "";
            std::string paramVal = "";

            if (val[i].isMember("param")) {
                paramName = val[i]["param"].asString();
            }

            if (val[i].isMember("value")) {
                paramVal = val[i]["value"].asString();
            }

            if ((paramName != "") && (paramVal != "")) {
                mapRef.insert({paramName, paramVal});
            }
        }
}