/*! This class adds the ability to parse radio config files
 *
 */

#ifndef RADIO_CONFIG_PARSER_HPP
#define RADIO_CONFIG_PARSER_HPP

#include <fstream>
#include <map>
#include "json.h"


namespace RadioConfigParser {
    void fillMap(std::map<std::string, std::string> &mapRef, Json::Value &val);
    bool parseConfig(std::map<std::string, std::string> &mapRef);
    bool getParamsFromFile(std::ifstream &file, Json::Value &root, Json::Value &params);
};


#endif