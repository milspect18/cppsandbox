#include "PrintServer.hpp"
#include <iostream>


bool PrintServer::handleData(uint8_t *data, uint16_t dataLen) {
    std::string dataStr = reinterpret_cast<char *>(data);

    // This is where we would do something meaningful with the data
    // that we got
    std::cout << dataStr << std::endl;

    return true;
}