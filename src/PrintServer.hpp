#pragma once

#include "TcpServer.hpp"

class PrintServer : public KP_Network::TcpServer {
    public:
        PrintServer() = default;
        PrintServer(std::string ip, uint16_t port) : TcpServer(ip, port) {};
        bool handleData(uint8_t *data, uint16_t numBytes) override;
};
