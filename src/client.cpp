
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <arpa/inet.h>


typedef unsigned char byte;


void error(const char *msg);


int main(int argc, const char *argv[]) {
    int sockFd, bytesWritten;
    struct sockaddr_in serverAddr;

    sockFd = socket(AF_INET, SOCK_STREAM, 0);
    
    if (sockFd < 0) {
        error("Got an invalid socket fd");
    }

    serverAddr.sin_family = AF_INET;
    inet_pton(AF_INET, "127.0.0.1", &(serverAddr.sin_addr));
    serverAddr.sin_port = htons(65535);

    if (connect(sockFd, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0) {
        error("Unable to connect!");
    }

    if (argc > 1) {
        bytesWritten = write(sockFd, argv[1], strlen(argv[1]));
    } else {
        bytesWritten = write(sockFd, "Hello!", 6);
    }

    if (bytesWritten < 0) {
        error("Unable to write to sockFd!");
    }

    close(sockFd);
}


void error(const char *msg)
{
    perror(msg);
    exit(1);
}
