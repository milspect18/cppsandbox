/*! Simple driver for the cppSandbox
 *
 *  We use this as the main entry point for driving
 *  all code written in the cppSandbox repo
 */


#include "PrintServer.hpp"


////
int main(int argc, char const *argv[]) {
    PrintServer server;

    server.startServing();

    return 0;
}
